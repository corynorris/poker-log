﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace PokerLog
{
    public partial class Form1 : Form
    {
        private string SaveLoc = "c:/PokerSaves/";
        private string SaveName = "Saves";
        public Form1()
        {
            InitializeComponent();
        }

         private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Enabled = checkBox1.Checked;
        }

         private void numericUpDown1_ValueChanged(object sender, EventArgs e)
         {
             lblProfitLoss.Text = (numericUpDown2.Value - numericUpDown1.Value).ToString(); 
         }

         private void numericUpDown2_ValueChanged(object sender, EventArgs e)
         {
             lblProfitLoss.Text = (numericUpDown2.Value - numericUpDown1.Value).ToString(); 
         
         }

         private void button1_Click(object sender, EventArgs e)
         {            

             if (!checkBox1.Enabled)
                 textBox1.Text = "";

             Event evnt = new Event(dateTimePicker1.Value, numericUpDown1.Value, numericUpDown2.Value, textBox1.Text);

             MessageBox.Show("Event added: " + evnt.ToString(),"Success");

             listBox1.Items.Add(evnt);
             lblNet.Text = calcNet().ToString();
            
         }
         private void save()
         {             
             StreamWriter sw = new StreamWriter(SaveLoc + SaveName);
             foreach (Event item in listBox1.Items)
             {
                 sw.WriteLine(item.writeSave());
             }
             sw.Close();
             MessageBox.Show("Saved");
         }
         private void saveToolStripMenuItem_Click(object sender, EventArgs e)
         {
             save();
         }

         private void Form1_Load(object sender, EventArgs e)
         {
             if (!Directory.Exists(SaveLoc))
                 Directory.CreateDirectory(SaveLoc);
             else
             {
                 try
                 {
                     StreamReader sr = new StreamReader(SaveLoc + SaveName);
                     string line = sr.ReadLine();
                     while (line != null)
                     {
                         listBox1.Items.Add(new Event(line));
                         line = sr.ReadLine();
                     }
                     sr.Close();
                     lblNet.Text = calcNet().ToString();
                 }
                 catch (Exception er)
                 {
                     MessageBox.Show("Something has happened that caused the save file in c:/PokerSaves to be deleted", "Data Lost");
                 }
             }

         }

         private void exportToolStripMenuItem_Click(object sender, EventArgs e)
         {
             if (DialogResult.OK == saveFileDialog1.ShowDialog())
             {
                 try
                 {

                     StreamWriter sw = new StreamWriter(saveFileDialog1.FileName + ".txt");
                     foreach (Event item in listBox1.Items)
                     {
                         sw.WriteLine(item);
                     }
                     sw.Close();
                     MessageBox.Show("Saved");
                 }
                 catch (Exception er)
                 {
                     MessageBox.Show(er.Message + " please send me an email explaining what you were doing and with this error name", "The following error has occured");

                 }
             }
         }
         private decimal calcNet()
         {
             decimal toReturn = 0;
             foreach (Event evnt in listBox1.Items)
             {
                 toReturn += evnt.getNet();
             }
             return toReturn;
         }
         private void listBox1_DoubleClick(object sender, EventArgs e)
         {

             if (DialogResult.OK == MessageBox.Show("Are you sure you want to delete this item?\n" + listBox1.SelectedItem.ToString(), "Confirm Delete", MessageBoxButtons.OKCancel))
             {
                 listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                 lblNet.Text = calcNet().ToString();
             }
         

             
         }

         private void Form1_FormClosing(object sender, FormClosingEventArgs e)
         {
             DialogResult dr = MessageBox.Show("Save before closing?", "Save Changes?", MessageBoxButtons.YesNoCancel);
             if (DialogResult.Cancel == dr)
                 e.Cancel = true;
             else if (DialogResult.Yes == dr)
                 save();

       

         }



    

    }
}
