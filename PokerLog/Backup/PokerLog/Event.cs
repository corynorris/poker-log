﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokerLog
{
    class Event
    {
        private DateTime dtDate;
        private decimal dStart, dEnd;
        private String strTicket;

 

        public Event(DateTime date, decimal start, decimal end, String ticket)
        {
            this.dtDate = date;
            this.dStart = start;
            this.dEnd = end;
            if (ticket == "")
                this.strTicket = "n/a";
            else
                this.strTicket = ticket;
        }
        public Event(String str)
        {
            string[] parts = str.Split(',');
            dtDate = DateTime.Parse(parts[0]);
            dStart = decimal.Parse(parts[1]);
            dEnd = decimal.Parse(parts[2]);
            strTicket = parts[3];
        }

        public override string  ToString()
        {
            string toReturn = "Date: " + dtDate.ToShortDateString() + "\tStarting Cash: " + dStart.ToString() + "\tEnding Cash: " + dEnd.ToString() + "\tTicket: " + strTicket;

            
                

     /*
            if (profitLoss >= 0)
                toReturn += "\tProfit: ";
            else
                toReturn += "\tLoss: ";

            toReturn += profitLoss.ToString();*/


            return toReturn;

        }

        public string writeSave()
        {
            return dtDate.ToShortDateString() + "," +
                dStart.ToString() + "," + dEnd.ToString() + "," +
                strTicket;

        }
        public decimal getNet()
        {
            decimal toReturn = (dEnd - dStart);
            return toReturn;
        }
    }
}
